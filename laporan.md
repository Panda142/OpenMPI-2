# Laporan Praktikum IF3230 Sistem Paralel dan Terdistribusi

## Praktikum OpenMPI topik Quicksort

### 1. Deskripsi Solusi

Pada program ini ada dua fitur, yaitu jika OpenMPI dideklarasikan hanya 1 proses maka akan menjalankan serial quicksort, namun jika lebih akan menjalankan paralel quicksort. Ini digunakan dengan kode berikut :

```c
    if(nodenum == 1)
	{
		// Run serial quicksort		
		quicksort(dataset,0,N-1);
		end = MPI_Wtime();
		printf("time is  %lf s\n",(double)(end - start));
	}
	else
	{
		// Run paralel quicksort
		srand (time(NULL));
		pivot_index = rand()%N;
		pivot = dataset[pivot_index];
		parallel_quicksort_plus(dataset,N,pivot,MPI_COMM_WORLD);

		end = MPI_Wtime();
		if(node == 0)
		{
			printf("time is  %lf s\n",(double)(end - start));
		}

	}
```

Pada algoritma serial quicksort tidak akan dibahas, yang dibahas yaitu bagian paralel. Pertama kali dilakukan pemilihan pivot. Pemilihan pivot dilakukan secara random.

Lalu pertama yang dilakukan yaitu mendapat prefix dari setiap proses dan akan dikumpulkan pada proses 0. Misalkan ada 6 buah elemen dan 3 buah proses. Masing-masing akan mendapat 2 buah elemen yang akan dicari prefixnya. Proses 0 mendapat dari awal yaitu 0 dan 1, proses 1 yaitu 2 dan 3, proses 2 yaitu 4 dan 5. Berikut kode yang melakukan hal tersebut :

```c
if(node!=nodenum-1)
	{
		currentdatalength = length/nodenum;		
		prefix = parallel_quicksort(dataset,pivot,length/nodenum*node,length/nodenum*(node+1)-1);	
	}
	else
	{
		currentdatalength = length - length/nodenum*(nodenum-1);
		prefix = parallel_quicksort(dataset,pivot,length/nodenum*node,length-1);
	}
	
MPI_Gather(&prefix , 1, MPI_INT,recvPrefix,1,MPI_INT,0,comm);

prefixplus = 0;

	if(node == 0)
	{
		prefixplus = recvPrefix[0];
		memcpy(recvnum,dataset,prefix*sizeof(int));
		for(i=1;i<nodenum;i++)
		{
			MPI_Recv(recvnum+prefixplus, recvPrefix[i], MPI_INT, i,0,comm,&status);
			prefixplus+=recvPrefix[i];
		}
	}
	else
	{
		MPI_Send(dataset+length/nodenum*node,prefix,MPI_INT,0,0,comm);
	}
```

Setelah mendapatkan prefix dan membagi dua bagian. Selanjutnya membagi dua proses grup, grup 1 akan mengurusi bilangan kecil dan grup 2 mengurusi bilangan besar. Dilakukan seperti berikut :
```c
if(node<smallP)
		MPI_Comm_split(orginalComm,0,node,&smallComm);
	else
		MPI_Comm_split(orginalComm,1,node,&smallComm);
```

### 2. Analisis Solusi

Ada beberapa hal yang mungkin terjadi pada algoritma ini dan memberhentikan iterasi. 

1. Jumlah elemen sudah 1. 
2. Jumlah proses tidak bisa dibagi lagi. Maka akan dilakukan quicksort yang konkuren pada proses tersebut.
3. Tidak ada pembagian proses pada elemen dengan bilangan kecil. Melakukan quicksort konkuren pada elemen tersebut.
4. Hanya ada 1 atau 0 elemen pada elemen dengan bilangan kecil (lebih kecil dari pivot) sehingga tidak perlu membandingkan atau iterasi pada elemen tersebut.

Agar mendapatkan hasilnya melalui proses berikut :

```c
    MPI_Bcast(smallDataset,prefixplus,MPI_INT,0,comm);
	MPI_Bcast(largeDataset,length-prefixplus,MPI_INT,nodenum-1,comm);

 	memset(dataset,0,length*sizeof(int));
 	memcpy(dataset,smallDataset,prefixplus*sizeof(int));
 	memcpy(dataset+prefixplus,largeDataset, (length-prefixplus)*sizeof(int));

```

Proses ini menggabungkan data set kecil dan besar (data yang dibagi oleh pivot).

### 3. Jumlah Proses yang digunakan

Adapun jumlah proses yang digunakan yaitu 20 proses. Setelah diuji melihat hasil waktunya, 20 proses terlihat cukup memberikan hasil yang baik.

### 4. Pengukuran Performansi

![Perbedaan](hasil.png)

Pada gambar tersebut terlihat hasil pengukuran performansi. Pada pemanggilan 20 proses merupakan paralel, namun untuk 1 sebagai serial.

### 5. Analisis Perbandingan

Pada hasil pengukuran poin 4, terlihat performa serial masih unggul dibandingkan paralel. Ada beberapa faktor yang menyebabkan ini. Dikarenakan server digunakan oleh banyak orang, performa yang diberikan server terlihat kurang baik. Terkadang paralel bisa memberikan hasil lebih cepat.

Namun faktor ini bukan hal utama. Hal utama yang terjadi karena jumlah yang ditangani masih terbilang lebih sedikit untuk dibagi dengan metode yang diberikan. Jika jumlah lebih banyak, dimungkinkan untuk lebih cepat dibandingkan yang serial. Seringnya terjadi komunikasi pada jumlah elemen yang sedikit akan memperburuk kecepatan.


### About

Bervianto Leo P - 13514047

### Referensi

[github.com/cloverfisher/MPIquicksort](https://github.comcloverfisher/MPIquicksort)
