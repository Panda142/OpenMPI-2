#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h> 

int quicksort(int *dataset, int q, int r);
int changedata(int *a, int *b);
int parallel_quicksort(int *dataset,int pivot,int q,int r);
int parallel_quicksort_plus(int *dataset, int length, int pivot,MPI_Comm comm);
int prifixsum = 0;
double start,end;


int main(int argc, char** argv)
{
	if (argc!=2) {
		fprintf(stderr, "Usage: quick_sort [N Data]\n");
		exit(1);
	}

	int i;
	int prefix;
	int pivot_index;
	int *dataset;
	int pivot = 0;
	
	int nodenum,node;
	
	/* Initial Element of Array*/
	int N;
	N = atoi(argv[1]);	
	dataset = (int *) malloc (N*sizeof(int));
	srand(time(NULL));
	
	for(i=0;i<N;i++)
	{
		dataset[i] = rand() % (2*N);	
	}
	
	/* Initial MPI */
	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&node);
	MPI_Comm_size(MPI_COMM_WORLD,&nodenum);
	
	if(node == 0)
	{
		start = MPI_Wtime();
	}

	if(nodenum == 1)
	{
		// Run serial quicksort		
		quicksort(dataset,0,N-1);
		end = MPI_Wtime();
		printf("time is  %lf s\n",(double)(end - start));
	}
	else
	{
		// Run paralel quicksort
		srand (time(NULL));
		pivot_index = rand()%N;
		pivot = dataset[pivot_index];
		parallel_quicksort_plus(dataset,N,pivot,MPI_COMM_WORLD);

		end = MPI_Wtime();
		if(node == 0)
		{
			printf("time is  %lf s\n",(double)(end - start));
		}

	}
	free(dataset);

	MPI_Finalize();
	return 0;
}

int parallel_quicksort_plus(int *dataset, int length, int pivot,MPI_Comm comm)
{
	int i;
	int nodenum,node,nodeplus;
	int prefix = 0;
	int *recvnum;
	int recvPrefix[20];
	int prefixplus = 0;
	int smallP=0;
	int currentdatalength = 0;
	int largePivot = 0,smallPivot=0;
	int smallLength,largeLength;
	int *smallDataset,*largeDataset;
	MPI_Comm smallComm,largeComm;
	MPI_Comm orginalComm;
	MPI_Status status;
	

	MPI_Comm_rank(comm,&node);
	MPI_Comm_size(comm,&nodenum);
	
	if(nodenum == 1)
	{
		quicksort(dataset,0,length-1);
		return 0;
	}

	recvnum = (int *)malloc(length*sizeof(int));

	if(node!=nodenum-1)
	{
		currentdatalength = length/nodenum;		
		prefix = parallel_quicksort(dataset,pivot,length/nodenum*node,length/nodenum*(node+1)-1);	
	}
	else
	{
		currentdatalength = length - length/nodenum*(nodenum-1);
		prefix = parallel_quicksort(dataset,pivot,length/nodenum*node,length-1);
	}

	MPI_Gather(&prefix , 1, MPI_INT,recvPrefix,1,MPI_INT,0,comm);
	
	prefixplus = 0;

	if(node == 0)
	{
		prefixplus = recvPrefix[0];
		memcpy(recvnum,dataset,prefix*sizeof(int));
		for(i=1;i<nodenum;i++)
		{
			MPI_Recv(recvnum+prefixplus, recvPrefix[i], MPI_INT, i,0,comm,&status);
			prefixplus+=recvPrefix[i];
		}
	}
	else
	{
		MPI_Send(dataset+length/nodenum*node,prefix,MPI_INT,0,0,comm);
	}

	if(node == 0)
	{
		memcpy(recvnum+prefixplus,dataset+prefix,(currentdatalength - prefix)*sizeof(int));
		for(i=1;i<nodenum;i++)
		{
			prefixplus += length/nodenum - recvPrefix[i-1];
			if(i!=nodenum-1)
				MPI_Recv(recvnum+prefixplus,length/nodenum-recvPrefix[i],MPI_INT,i,0,comm,&status);
			else
				MPI_Recv(recvnum+prefixplus,length - length/nodenum*(nodenum-1)-recvPrefix[i],MPI_INT,i,0,comm,&status);
		}
	}
	else
		MPI_Send(dataset+length/nodenum*node + prefix, currentdatalength - prefix, MPI_INT , 0, 0, comm);

	if(node == 0)
	{
		prefixplus = 0;
		for(i=0;i<nodenum;i++)
		{
			prefixplus += recvPrefix[i];
		}
		smallP = prefixplus * nodenum / length + 0.5;		

	}

	MPI_Bcast(&smallP,1,MPI_INT,0,comm);
	MPI_Bcast(&prefixplus,1,MPI_INT,0,comm);
	MPI_Bcast(recvnum,length,MPI_INT,0,comm);
	smallDataset = (int *)malloc(prefixplus*sizeof(int));
	largeDataset = (int *)malloc((length-prefixplus)*sizeof(int));

	if(node == 0)
	{
		memcpy(smallDataset,recvnum,prefixplus*sizeof(int));
		memcpy(largeDataset,recvnum+prefixplus,(length-prefixplus)*sizeof(int));	
	}

	MPI_Bcast(smallDataset,prefixplus,MPI_INT,0,comm);
	MPI_Bcast(largeDataset,length-prefixplus,MPI_INT,0,comm);

	orginalComm = comm;

	if(node<smallP)
		MPI_Comm_split(orginalComm,0,node,&smallComm);
	else
		MPI_Comm_split(orginalComm,1,node,&smallComm);

	if(smallP>nodenum-1)
	{
		quicksort(largeDataset,0,length-prefixplus-1);
	}

	if(smallP == 0)
	{
		quicksort(smallDataset,0,prefixplus-1);
	}

	if(node ==0)
	{
		if(prefixplus > 1)
 	 	{
 	  	 	smallPivot = rand()%prefixplus;
 	 		smallPivot = smallDataset[smallPivot];
 	 	}
 	 	if(prefixplus < length-1)
 	 	{
 	  	 	largePivot = rand()%(length - prefixplus);
 	 		largePivot = largeDataset[largePivot];
 	 	}
	}
	MPI_Bcast(&smallPivot,1,MPI_INT,0,comm);
	MPI_Bcast(&largePivot,1,MPI_INT,0,comm);
	
	if(node < smallP)
 	{
 	 	if(prefixplus >1)
 	 	{
 	 	 	parallel_quicksort_plus(smallDataset, prefixplus, smallPivot,smallComm);		
 	 	}

 	}
 	else
 	{
 		if(prefixplus < length-1)
 	 	{
			parallel_quicksort_plus(largeDataset, length-prefixplus,largePivot, smallComm);
 	 	}
 	}

 	MPI_Bcast(smallDataset,prefixplus,MPI_INT,0,comm);
	MPI_Bcast(largeDataset,length-prefixplus,MPI_INT,nodenum-1,comm);

 	memset(dataset,0,length*sizeof(int));
 	memcpy(dataset,smallDataset,prefixplus*sizeof(int));
 	memcpy(dataset+prefixplus,largeDataset, (length-prefixplus)*sizeof(int));

	free(recvnum);
	free(smallDataset);
	free(largeDataset);
	return 0;
}

int changedata(int *a, int *b)
{
	int temp;
	temp = *a;
	*a = *b;
	*b = temp;
	return 0;
}

int parallel_quicksort(int *dataset,int pivot,int q,int r)
{
	int x,s;
	int i;
	if(r<=q)
	{
		return 1;
	}
	else
	{
		x=pivot;
		s = q;
		for(i=q;i<=r;i++)
		{
			if(dataset[i] <= x)
			{
				changedata(&dataset[i],&dataset[s]);
				s = s+1;
			}
		}
	}
	return s-q;
}

int quicksort(int *dataset, int q, int r)
{
	int x,s;
	int i,j,k;
	int tempdata;
	if(r<=q)
	{
		return 1;
	}
	else
	{
		x = dataset[q];
		s = q;
		for(i=q+1;i<=r;i++)
		{
			if (dataset[i]<x)
			{
				s = s+1;
				tempdata = dataset[s];
				dataset[s] = dataset[i];
				dataset[i] = tempdata;
			}
		}
		tempdata = dataset[q];
		dataset[q] = dataset[s];
		dataset[s] = tempdata;
		quicksort(dataset,q,s);
		quicksort(dataset,s+1,r);
	}
	return 0;
}



